import base64
import httplib2
import urllib, mimetypes
import os.path
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from apiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
SCOPES = ['https://www.googleapis.com/auth/gmail.send']
# Path to the client_secret.json file downloaded from the Developer Console
def send_mail(img):
	creds = None
	if os.path.exists('token.pickle'):
	    with open('token.pickle', 'rb') as token:
	        creds = pickle.load(token)
	# If there are no (valid) credentials available, let the user log in.
	if not creds or not creds.valid:
	    if creds and creds.expired and creds.refresh_token:
	        creds.refresh(Request())
	    else:
	        flow = InstalledAppFlow.from_client_secrets_file(
	            'credentials.json', SCOPES)
	        creds = flow.run_local_server()
	    # Save the credentials for the next run
	    with open('token.pickle', 'wb') as token:
	        pickle.dump(creds, token)

	gmail_service = build('gmail', 'v1', credentials=creds)
	message = MIMEMultipart()
	# ,"soniprashil64@gmail.com"
	recipients = ["ahirjagdish05@gmail.com","katariyajagdish05@gmail.com"]
	message['to'] = ",".join(recipients)
	# message['to'] = "ahirjagdish05@gmail.com"
	message['from'] = "vgecit2017@gmail.com"
	message['subject'] = "Human Detection"
	# msg = MIMEText("message_text")
	# message.attach(msg)
	# file="2019.jpg"
	file=img
	content_type, encoding = mimetypes.guess_type(file)
	print(content_type)
	# if content_type is None or encoding is not None:
	# content_type = 'application/octet-stream'
	main_type, sub_type = content_type.split('/', 1)
	# print(sub_type)
	# print(main_type)
	# fp = open(file, 'rb')
	with open(file, 'rb') as f:
	  # contents = f.read()
		msg = MIMEImage(f.read(), _subtype=sub_type)
		# print(msg)
		f.close()
	filename = os.path.basename(file)
	print(type(filename))
	text11=str(filename).split('.', 1)
	text1 = MIMEText(str(text11[0]))
	print(text1)
	message.attach(text1)
	msg.add_header('Content-Disposition', 'attachment', filename=filename)
	message.attach(msg)
	#return {'raw': base64.urlsafe_b64encode(message.as_string())}
	body= {'raw': base64.urlsafe_b64encode(message.as_string().encode('UTF-8')).decode('ascii')}
	message = (gmail_service.users().messages().send(userId="me", body=body).execute())
	print('Message Id: %s' % message['id'])
	# print(message)
# send_mail("2019.jpg")