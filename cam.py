# Code adapted from Tensorflow Object Detection Framework
# https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb
# Tensorflow Object Detection Detector

import numpy as np
import tensorflow as tf
import cv2
import time
from datetime import datetime
import gmail
## ----------------------------- Firebase ------------------------
import pyrebase
config = {
  "apiKey": "apiKey",
  "authDomain": "home-1823d.firebaseapp.com",
  "databaseURL": "https://home-1823d.firebaseio.com/",
  "storageBucket": "home-1823d.appspot.com"
}
firebase = pyrebase.initialize_app(config)
storage = firebase.storage()
db = firebase.database()
fmt = '%Y-%m-%d %H:%M:%S'
i = 0
time_img_detect = None
check = True
text = "Human Detected"
people=False
percentage=0.0
class DetectorAPI:
    def __init__(self, path_to_ckpt):
        self.path_to_ckpt = path_to_ckpt

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.default_graph = self.detection_graph.as_default()
        self.sess = tf.Session(graph=self.detection_graph)

        # Definite input and output Tensors for detection_graph
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

    def processFrame(self, image):
        # Expand dimensions since the trained_model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image, axis=0)
        # Actual detection.
        start_time = time.time()
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})
        end_time = time.time()

        # print("Elapsed Time:", end_time-start_time)

        im_height, im_width,_ = image.shape
        boxes_list = [None for i in range(boxes.shape[1])]
        for i in range(boxes.shape[1]):
            boxes_list[i] = (int(boxes[0,i,0] * im_height),
                        int(boxes[0,i,1]*im_width),
                        int(boxes[0,i,2] * im_height),
                        int(boxes[0,i,3]*im_width))

        return boxes_list, scores[0].tolist(), [int(x) for x in classes[0].tolist()], int(num[0])

    def close(self):
        self.sess.close()
        self.default_graph.close()

if __name__ == "__main__":
    model_path = 'frozen_inference_graph.pb'
    odapi = DetectorAPI(path_to_ckpt=model_path)
    threshold = 0.7
    cap = cv2.VideoCapture(0)

    while True:
        people=False
        time_current = datetime.strptime(datetime.today().strftime(fmt), fmt)

        time.sleep(0.5)
        r, img = cap.read()
        # img = cv2.resize(img, (1280, 720))
        # img = cv2.resize(img, (800, 450))


        boxes, scores, classes, num = odapi.processFrame(img)
        # Visualization of the results of a detection.
        for i in range(len(boxes)):
            # Class 1 represents human
            # print(scores[i],classes[1])
            if classes[i] == 1 and scores[i] > threshold:
                print("\n")
                # people=True
                print(scores[i]*100,classes[1])
                box = boxes[i]
                cv2.rectangle(img,(box[1],box[0]),(box[3],box[2]),(255,0,0),2)
                percentage=scores[i]
                # print("People Detected",time_current)
                if time_img_detect != None:
                    diff = time_current - time_img_detect
                    diff_minutes = (diff.days * 24 * 60) + (diff.seconds/60)
                    update = abs(diff_minutes)

                    m1=diff_minutes/0.6
                    m2=diff_minutes%0.6
                    # print(m1)
                    # print(m2)
                    print("Waiting for next interval ",str(round(m1,2))+" Minutes ")
                    if update >= 2:
                        print("Time Differenec "+str(update))
                        print("update True")
                        check = True
                # if percentage >= 0.64:
                    # if check == True:
                        # if index == 1 :
                            # i = i + 1
                            # if i == 6 :
                if check == True:
                    time_img_detect = datetime.strptime(datetime.today().strftime(fmt), fmt)
                    print("Human Detect at: "+str(time_img_detect))
                    # print("There are Human")
                    # img = cv2.imwrite("./Detect Image/image.png",original_img)
                    # dimg= cap.read()[1]
                    # print(dimg)
                    # images_name = datetime.now().strftime('%Y%m%d_%Hh%Mm%Ss%f')
                    img=cv2.rectangle(img,(box[1],box[0]),(box[3],box[2]),(255,0,0),2)
                    path_Name = "./Detect_Image/"+str(time_img_detect)+ ".jpg"
                    height, width = img.shape[:2]
                    # print(x)
                    # print(width)
                    img = cv2.resize(img,(int(width*0.8),int(height*0.8)))
                    cv2.imwrite(path_Name, img,[int(cv2.IMWRITE_JPEG_QUALITY), 70])
                    # image_fullsize.save(filepath+name_fullsize, quality=95)
                    # path_Name = "./Detect_Image/"+datetime.now().strftime('%Y%m%d_%Hh%Mm%Ss%f') + ".jpg"
                    # print(path_Name)
                    # try:
                    print("Uploading image ..............")
                    # storage.child("images/"+str(time_img_detect)+".jpg").put(path_Name)
                    # db.child("images_name").child(images_name).set(str(percentage))
                    percentage_text = str(percentage)
                    # result_text = str(name_list[index])
                    data = {"percentage": percentage_text ,"result":"People"}
                    print("Uploading text ..............")
                    # print(time_img_detect)
                    # db.child("images_name").child(str(time_img_detect)).update(data)
                    # print()
                    gmail.send_mail(path_Name)
                    check = False

        cv2.imshow("preview", img)
        key = cv2.waitKey(1)
        if key & 0xFF == ord('q'):
            break
cap.release()